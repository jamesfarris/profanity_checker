<?php
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Check profanity</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
	<script>
		$(document).ready(function(){
			$('#profcheck').on('submit', function(){
				var form = $(this);
				console.log(form.seriize);
				$.get(
					form.attr('action'),
					form.serialize(),
					function(data){
						if (data.error) {
							$('#output').html('<span class="error">' + data.error + '</span>');
						}
						else
						{
							$('#output').text(data.output);
						}
					},
				"json"
				);
			return false;	
			});
		});
	</script>
	<style>
		.topmargin {
			margin-top: 5px;
		}
		#output {
			padding:15px;
		}
	</style>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
			    <div class="navbar-header">
					<a href="#" class="navbar-brand">Online Profanity Checker v1.0</a>
			    </div>
		    	<ul class="nav nav-pills topmargin">
		    		<li><a href="https://github.com/jamesf408/profanity_checker">View on GitHub</a></li>
		    		<li><a href="http://www.jamesafarris.info/projects/php/profanitychecker/profanitychecker.php">Profanity Checker API</a></li>
		    		<li><a href="http://www.jamesafarris.info">Additonal Projects</a></li>
		    	</ul>
			</div>
		</nav>

		<form id="profcheck" action="profanitychecker.php" method="get">
			<div class="form-group">
				<label class="col-sm-5 control-label" for="json">Enter text to be evaluated:</label>
				<textarea class="form-control" rows="8" name="json" id="json"></textarea>
			</div>
			<div class="form-group">
				<label lass="col-sm-5 control-label" for="options">Replace profanity with:</label>
				<input class="form-control" type="text" name="options" id="options" value="[censored]">
				<label><small>i.e "What the [censored]"</small></label>
			</div>
			<div class="form-group">
				<input class="btn btn-primary" type="submit" value="Check For Profanity"> <input class="btn btn-primary" type="reset" value="Clear Form">
			</div>
		</form>
		<label class="outputTitle">Cleaned Text:</label>
		<div class="cleaned">
			<pre id="output"></pre>
		</div>
		<nav class="navbar navbar-default">
			<div class="container">
			    <div class="navbar-header">
					<a href="#" class="navbar-brand">Online Profanity Checker v1.0</a>
			    </div>
		    	<ul class="nav nav-pills topmargin">
		    		<li><a href="https://github.com/jamesf408/profanity_checker">View on GitHub</a></li>
		    		<li><a href="http://www.jamesafarris.info/projects/php/profanitychecker/profanitychecker.php">Profanity Checker API</a></li>
		    		<li><a href="http://www.jamesafarris.info">Additonal Projects</a></li>
		    		<li class="navbar-text pull-right">&copy; 2014 James Farris</li>
		    	</ul>
			</div>
		</nav>
	</div>
</body>
</html>